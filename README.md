# pack-bees

## Synopsis :
Packer file to build a AWS Amazon Linux 2 AMI with bees-with-machine-guns installed and ready to run.

### Pre-requisites :
* must have your AWS_ACCESS_KEY and AWS_SECRET_ACCESS_KEY set as env variables.

### to Build :
```
# packer build pack-bees-ec2-amzn-linux2-ami.json
```

### Create Bees and Attack :
* create an EC2 instance from the AMI - the 'beeController'

```
# aws ec2 run-instances \
    --image-id <UPDATE THIS FROM THE OUTPUT OF YOUR PACKER-BUILD> \
    --key-name <UPDATE THIS TO YOUR SSH_PRIVATE_KEY_NAME> \
    --instance-type t2.micro \
    --query "Instances[0].InstanceId" \
    --output text
```

* SSH to the ‘beeController’ instance that you created using the public DNS name or IP address

```
# ssh -i key.pem ec2-user@<ec2-hostname>
```

#### Create bee-swarm :
  * -s is the number of BEE micro instances
  * -k is the SSH privatekey that was used to create the 'beeController' 

```
# bees up -s 2 -k myprivatekey
```

#### Check your swarm :
Check if the BEE instances were created, they are all named ‘a bee!’, one micro instance for each bee.

```
# aws ec2 describe-instances --query 'Reservations[].Instances[].[Tags[?Key==Name].Value | [0], InstanceId, Placement.AvailabilityZone, InstanceType, LaunchTime, State.Name, PublicDnsName]'
```

#### Attack a site: 
  * -n is Total number of connections
  * -c is Concurrent number of connection per bee
  * -k is the SSH privatekey that was used to create the 'beeController'
  * -u the Target site

```
# bees attack -n 10000 -c 250 -k myprivatekey -u http://mysite/index.html
```

You should notice http traffic hitting the server hosting this website

#### Stop attack :
* Shut Down Bees micro instances:

```
# bees down
```

### Disclaimer :
Launching ‘ Bees with Machine Guns ‘ means launching DDOS attack. Any DDOS attack will face legal action. <br>
*This tool only can be used for testing YOUR site*

### Author :
@shallawell